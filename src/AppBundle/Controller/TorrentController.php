<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 14:31
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TorrentController extends Controller
{
    public function torrentAction(Request $request)
    {
        $torrentApiService = $this->get('api_action_factory');
        $apiAction = $torrentApiService->getByHttpMethod($request->getMethod());
        $response = $apiAction->doRequest($request);
        return new JsonResponse(json_encode($response));
    }

    public function getStatusAction(Request $request)
    {
        $torrentApiService = $this->get('api_action_factory');
        $apiAction = $torrentApiService->getByHttpMethod($request->getMethod());
        $response = $apiAction->doRequest($request);
        dump($response);die;
        return new JsonResponse(json_encode($response));

    }
}