<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 14:17
 */

namespace AppBundle\Exception;


class MissingFieldResponseException extends \Exception
{
    protected $code = -1;
    protected $message;

    /**
     * MissingFieldResponseException constructor.
     * @param string $missingFieldName
     */
    public function __construct(string $missingFieldName)
    {
        $this->message = 'The field ' . $missingFieldName . ' is missing.';
    }


}