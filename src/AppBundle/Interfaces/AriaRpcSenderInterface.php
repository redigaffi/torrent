<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 14:48
 */

namespace AppBundle\Interfaces;

interface AriaRpcSenderInterface
{
    public function sendAriaRpcRequest(RequestInterface $request): ResponseInterface;
}