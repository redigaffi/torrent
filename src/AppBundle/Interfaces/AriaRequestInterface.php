<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 13:03
 */

namespace AppBundle\Interfaces;


interface AriaRequestInterface
{
    public function getUrl(): string;
    public function getMethod(): string;
}