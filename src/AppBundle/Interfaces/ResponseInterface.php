<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 13:54
 */

namespace AppBundle\Interfaces;


interface ResponseInterface extends \JsonSerializable
{
    public static function fromArray(array $params);
}