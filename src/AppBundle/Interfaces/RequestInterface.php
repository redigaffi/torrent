<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 22/09/17
 * Time: 17:36
 */

namespace AppBundle\Interfaces;


interface RequestInterface
{
    public function getParams(): array;
    public function getMethod(): string;
}