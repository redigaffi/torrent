<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 24/09/17
 * Time: 15:50
 */

namespace AppBundle\Interfaces;


use Symfony\Component\HttpFoundation\Request;

interface ApiActionInterface
{
    public function doRequest(Request $request): ResponseInterface;
}