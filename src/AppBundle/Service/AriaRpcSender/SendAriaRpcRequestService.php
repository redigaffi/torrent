<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 12:52
 */

namespace AppBundle\Service\AriaRpcSender;

use AppBundle\Interfaces\AriaRpcSenderInterface;
use AppBundle\Interfaces\RequestInterface;
use AppBundle\Interfaces\ResponseInterface;
use AppBundle\Service\Factory\Response\ResponseFactory;
use AppBundle\ValueObject\ErrorCodes;
use AppBundle\ValueObject\Request\AriaRpcRequest;
use AppBundle\ValueObject\Response\EmptyResponse;
use GuzzleHttp\Client;

class SendAriaRpcRequestService implements AriaRpcSenderInterface
{
    private $httpClient;
    private $responseFactory;

    /**
     * SendAriaRpcRequestService constructor.
     * @param Client $httpClient
     * @param ResponseFactory $responseFactory
     */
    public function __construct($httpClient, ResponseFactory $responseFactory)
    {
        $this->httpClient = $httpClient;
        $this->responseFactory = $responseFactory;
    }


    public function sendAriaRpcRequest(RequestInterface $request): ResponseInterface
    {
        $ariaRpcRequest = AriaRpcRequest::fromRequestType($request);

        $response = $this->httpClient->post($ariaRpcRequest->getUrl(), [
            'body' => json_encode($ariaRpcRequest)
        ]);

        $responseContent = $response->getBody()->getContents();
        $parsedResponse = $this->responseFactory->getResponse($request, json_decode($responseContent, true));

        return $parsedResponse;
    }

}