<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 24/09/17
 * Time: 20:40
 */

namespace AppBundle\Service\ApiAction;


use AppBundle\Interfaces\ApiActionInterface;
use AppBundle\Interfaces\AriaRpcSenderInterface;
use AppBundle\Interfaces\ResponseInterface;
use AppBundle\ValueObject\ErrorCodes;
use AppBundle\ValueObject\Request\TellStatusRequest;
use AppBundle\ValueObject\Response\EmptyResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiActionGetService implements ApiActionInterface
{
    private $ariaRpcSender;

    /**
     * ApiActionGetService constructor.
     * @param $ariaRpcSender
     */
    public function __construct(AriaRpcSenderInterface $ariaRpcSender)
    {
        $this->ariaRpcSender = $ariaRpcSender;
    }


    public function doRequest(Request $request): ResponseInterface
    {
        $response = EmptyResponse::fromArray(['status' => ErrorCodes::RESPONSE_FAIL]);
        $torrentId = $request->get('id');

        $tellStatusRequest = new TellStatusRequest($torrentId);

        $response = $this->ariaRpcSender->sendAriaRpcRequest($tellStatusRequest);

        return $response;
    }


}