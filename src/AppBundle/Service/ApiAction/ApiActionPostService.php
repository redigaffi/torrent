<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 24/09/17
 * Time: 15:50
 */

namespace AppBundle\Service\ApiAction;


use AppBundle\Interfaces\ApiActionInterface;
use AppBundle\Interfaces\AriaRpcSenderInterface;
use AppBundle\Interfaces\ResponseInterface;
use AppBundle\ValueObject\ErrorCodes;
use AppBundle\ValueObject\Request\AddTorrentRequest;
use AppBundle\ValueObject\Response\EmptyResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiActionPostService implements ApiActionInterface
{
    private $ariaRpcSender;
    private $pathForUploadedFiles;

    /**
     * ApiActionPostService constructor.
     * @param AriaRpcSenderInterface $ariaRpcSender
     * @param string $pathForUploadedFiles
     */
    public function __construct(AriaRpcSenderInterface $ariaRpcSender, string $pathForUploadedFiles)
    {
        $this->ariaRpcSender = $ariaRpcSender;
        $this->pathForUploadedFiles = $pathForUploadedFiles;
    }


    public function doRequest(Request $request): ResponseInterface
    {
        $response = EmptyResponse::fromArray(['status' => ErrorCodes::RESPONSE_FAIL]);
        $files = $request->files->all();

        /**
         * @var File $file
         */
        foreach ($files as $file) {
            $fileName = uniqid('', true);
            $file->move($this->pathForUploadedFiles, $fileName);
            $torrentContent = file_get_contents($this->pathForUploadedFiles .'/'.$fileName);
            $torrentContentEncoded = base64_encode($torrentContent);
            $addTorrentRequest = new AddTorrentRequest($torrentContentEncoded);
            $response = $this->ariaRpcSender->sendAriaRpcRequest($addTorrentRequest);
        }

        return $response;
    }
}