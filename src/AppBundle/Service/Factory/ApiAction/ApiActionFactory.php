<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 24/09/17
 * Time: 15:47
 */

namespace AppBundle\Service\Factory\ApiAction;


use AppBundle\Interfaces\ApiActionInterface;
use Symfony\Component\HttpFoundation\Request;

class ApiActionFactory
{
    private $apiActionPost;
    private $apiActionGet;

    /**
     * ApiActionFactory constructor.
     * @param ApiActionInterface $apiActionPost
     * @param ApiActionInterface $apiActionGet
     */
    public function __construct(ApiActionInterface $apiActionPost, ApiActionInterface $apiActionGet)
    {
        $this->apiActionPost = $apiActionPost;
        $this->apiActionGet = $apiActionGet;
    }

    public function getByHttpMethod(string $requestMethod): ApiActionInterface
    {
        switch ($requestMethod) {
            case Request::METHOD_POST:
                return $this->apiActionPost;
            case Request::METHOD_GET:
                return $this->apiActionGet;

        }
    }
}