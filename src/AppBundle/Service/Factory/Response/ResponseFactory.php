<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 13:50
 */

namespace AppBundle\Service\Factory\Response;


use AppBundle\Interfaces\RequestInterface;
use AppBundle\Interfaces\ResponseInterface;
use AppBundle\ValueObject\Request\AddTorrentRequest;
use AppBundle\ValueObject\Request\TellStatusRequest;
use AppBundle\ValueObject\Response\AddTorrentResponse;
use AppBundle\ValueObject\Response\TellStatusResponse;

class ResponseFactory
{
    public function getResponse(RequestInterface $request, array $response): ResponseInterface
    {
        switch (get_class($request)) {
            case AddTorrentRequest::class:
                return AddTorrentResponse::fromArray($response);

            case TellStatusRequest::class:
                return TellStatusResponse::fromArray($response);


        }
    }
}