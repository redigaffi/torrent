<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 22/09/17
 * Time: 17:37
 */

namespace AppBundle\ValueObject\Request;


use AppBundle\Interfaces\RequestInterface;

class AddTorrentRequest implements RequestInterface
{
    const METHOD = 'addTorrent';
    private $torrent;

    /**
     * AddTorrentRequest constructor.
     * @param string $torrent This param is the file encoded in base64
     */
    public function __construct(string $torrent)
    {
        $this->torrent = $torrent;
    }

    public function getParams(): array
    {
        return [
            $this->torrent
        ];
    }

    public function getMethod(): string
    {
        return self::METHOD;
    }


}