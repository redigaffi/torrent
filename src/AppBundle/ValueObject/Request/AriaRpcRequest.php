<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 22/09/17
 * Time: 17:29
 */

namespace AppBundle\ValueObject\Request;


use AppBundle\Interfaces\AriaRequestInterface;
use AppBundle\Interfaces\RequestInterface;

class AriaRpcRequest implements AriaRequestInterface,\JsonSerializable
{

    const METHOD_PREFIX = 'aria2';
    const VERSION       = '2.0';
    const ID            = 'qwer';
    const PORT          =  5000;
    const URL           = 'http://localhost:'.self::PORT.'/jsonrpc';

    private $method;
    private $params;

    public function __construct(string $method, array $params)
    {
        $this->method = $method;
        $this->params = $params;
    }

    public static function fromRequestType(RequestInterface $request)
    {
        return new self($request->getMethod(), $request->getParams());
    }

    function jsonSerialize()
    {
        return [
            'jsonrpc'   => self::VERSION,
            'id'        => uniqid(),
            'method'    => self::METHOD_PREFIX .'.'.$this->method,
            'params'    => $this->params
        ];
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return self::URL;
    }
}