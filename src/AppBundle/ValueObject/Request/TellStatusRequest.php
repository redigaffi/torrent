<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 24/09/17
 * Time: 20:28
 */

namespace AppBundle\ValueObject\Request;


use AppBundle\Interfaces\RequestInterface;

class TellStatusRequest implements RequestInterface
{

    private $id;

    /**
     * TellStatusRequest constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }


    public function getParams(): array
    {
        return [
            $this->id
        ];
    }

    public function getMethod(): string
    {
        return 'tellStatus';
    }

}