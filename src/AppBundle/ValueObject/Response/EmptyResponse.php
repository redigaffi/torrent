<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 24/09/17
 * Time: 19:24
 */

namespace AppBundle\ValueObject\Response;


use AppBundle\Exception\MissingFieldResponseException;
use AppBundle\Interfaces\ResponseInterface;

class EmptyResponse implements ResponseInterface
{
    private $status;

    /**
     * EmptyResponse constructor.
     * @param string $status
     */
    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public static function fromArray(array $params)
    {
        if (!isset($params['status'])) {
            throw new MissingFieldResponseException('status');
        }

        return new self($params['status']);
    }

    function jsonSerialize()
    {
        return [
            'status' => $this->status
        ];
    }

}