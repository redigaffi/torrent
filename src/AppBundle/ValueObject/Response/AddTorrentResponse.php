<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 23/09/17
 * Time: 13:53
 */

namespace AppBundle\ValueObject\Response;


use AppBundle\Exception\MissingFieldResponseException;
use AppBundle\Interfaces\ResponseInterface;

class AddTorrentResponse implements ResponseInterface
{
    private $status;
    private $id;

    /**
     * AddTorrentResponse constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function fromArray(array $params)
    {
        if (!isset($params['result'])) {
            throw new MissingFieldResponseException('id');
        }

        return new self($params['result']);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->id
        ];
    }


}