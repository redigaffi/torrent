<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 12/10/17
 * Time: 13:44
 */

namespace AppBundle\ValueObject\Response;


use AppBundle\Interfaces\ResponseInterface;

class TellStatusResponse implements ResponseInterface
{
    private $downloaded;
    private $speed;
    private $totalLength;

    /**
     * TellStatusResponse constructor.
     * @param $downloaded
     * @param $speed
     * @param $totalLength
     */
    public function __construct(int $downloaded, int $speed, int $totalLength)
    {
        $this->downloaded = $downloaded;
        $this->speed = $speed;
        $this->totalLength = $totalLength;
    }


    public static function fromArray(array $params)
    {
        return new self((int) $params['result']['completedLength'],(int) $params['result']['downloadSpeed'],(int) $params['result']['totalLength']);
    }


    private function downloadSpeedAsMb()
    {
        $kiloBytes = $this->speed/1024;
        return $kiloBytes/1024;
    }

    function jsonSerialize()
    {
        return [
            'downloaded' => $this->downloaded,
            'speed' => $this->speed,
            'totalLength' => $this->totalLength
        ];
    }


}